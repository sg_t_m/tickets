<div class="cal3 popup-calendar">
    <script type="text/template" id="mini-clndr-template">
        <div class="clndr-controls top"><div class="clndr-previous-button">&lsaquo;</div><div class="clndr-next-button">&rsaquo;</div></div>
        <div class="clearfix">
        <% _.each(months, function(cal) { %>
            <div class="cal">
                <div class="clndr-controls"><div class="month"><%= cal.month.format('MMMM') %></div></div>
                <div class="clndr-grid"><div class="days-of-the-week"><div class="headers"><% _.each(daysOfTheWeek, function(day) { %><div class="header-day"><%= day %></div><% }); %></div><% _.each(cal.days, function(day) { %><div class="<%= day.classes %>"><%= day.day %></div><% }); %></div></div>
            </div>
        <% }); %>
        </div>
    </script>
</div>
