<div id="animatedModal<?php if(isset($back) && $back==true) echo 'Back'; ?>" data-nid="<?php echo $node->nid; ?>" data-date="<?php echo $date; ?>">
    <div class="close-animatedModal" id="closebt-container"><img class="closebt" src="/sites/all/modules/tickets/images/closebt.svg"></div>            
    <div class="modal-content tax-flight">
        <div class="tax-flight-info">
            <div><span>Номер рейса:</span> <b><?php echo $info->FlightNum; ?></b></div>
            <div><span>Дата вылета:</span> <b><?php echo format_date(strtotime($date) ,'custom','j F Y'); ?></b></div>
            <div><span>Класс бронирования:</span> <b>Y</b></div>
        </div>
        <h4 class="flight-info-h4"><?php echo $info->AirportFrom->name; ?> <i class="fa fa-long-arrow-right"></i> <?php echo $info->AirportTo->name; ?><br>Емкость: <?php echo $info->Places; ?>, <?php echo $info->TimeFrom; ?> - <?php echo $info->TimeTo; ?></h4>
        <div class="tax-flight-actions">
            <a href="#" data-nid="<?php echo $node->nid; ?>" data-date="<?php echo $date; ?>" class="pax-add form-submit">Добавить Пассажира</a>
            <a href="#" data-nid="<?php echo $node->nid; ?>" data-date="<?php echo $date; ?>" class="pax-import form-submit">Импорт из XLS</a>
            <a href="#" data-nid="<?php echo $node->nid; ?>" data-date="<?php echo $date; ?>" class="pax-export form-submit">Скачать в XLS</a>
        </div>
        <div class="tax-flight-passenger-add add-area" data-nid="<?php echo $node->nid; ?>" data-date="<?php echo $date; ?>">
            <h4>Добавить пассажира:</h4>
            <?php  
                module_load_include('inc', 'node', 'node.pages');
                $form = node_add('passenger');
                print drupal_render($form);
            ?>
        </div>
        <div class="tax-flight-passenger-add edit-area" data-nid="<?php echo $node->nid; ?>" data-date="<?php echo $date; ?>">
            <h4>Редактировать пассажира:</h4>
            <span class="edit-area-form"></span>
        </div>
        <div class="tax-passangers-table-inner" data-nid="<?php echo $node->nid; ?>" data-date="<?php echo $date; ?>"></div>
    </div>
</div>