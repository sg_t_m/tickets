<?php $agents = array(); ?>
<div class="error-import"></div>    
<table class="display flight-passangers-table" cellspacing="0" width="100%">
    <thead>
        <tr>
            <th class="no-sort">№</th>
            <th class="no-sort"></th>
            <th>Фамилия и имя</th>
            <th>Пол/возраст</th>
            <th>Пол</th>
            <th>Дата рождения</th>
            <th>Тип документа</th>
            <th>Номер документа</th>
            <th>Срок действия документа</th>
            <th>Резидент</th>
            <th>Обратный рейс</th>
            <th>Дата</th>
            <th>TKT</th>
            <?php if(!tickets_is_agent($user)): ?><th>Агент</th><?php endif; ?>
        </tr>
    </thead>
    <tbody>
        <?php if(count($data)): ?>
            <?php $num = 1; ?>            
            <?php foreach($data as $passenger): ?>
                <?php if(!tickets_is_agent($user)): ?>
                    <?php $agent = tickets_agent_load($passenger['uid']); ?>
                    <?php if(isset($agent->FullName) && !isset($agents[$agent->FullName])) $agents[$agent->FullName] = 0; ?>
                <?php endif; ?>
                <tr rel="<?php echo $passenger['fid']; ?>">
                    <td><?php echo $num; ?></td>
                    <td><input type="checkbox" name="passengers[]" value="<?php echo $passenger['nid']; ?>"></td>
                    <td><?php echo $passenger['title']; ?></td>
                    <td><?php echo $passenger['status']; ?></td>
                    <td><?php echo $passenger['sex']; ?></td>
                    <td><?php echo $passenger['birthday']; ?></td>
                    <td><?php echo $passenger['document']; ?></td>
                    <td><?php echo $passenger['num']; ?></td>
                    <td><?php echo $passenger['expire']; ?></td>
                    <td><?php echo $passenger['resident']->field_country_code['und'][0]['value']; ?></td>
                    <?php if($passenger['back']): ?><td><?php echo $passenger['backflight']; ?></td><?php endif; ?>
                    <?php if($passenger['back']): ?><td><?php echo $passenger['backdate']; ?></td><?php endif; ?>
                    <?php if($passenger['to']): ?><td><?php echo $passenger['toflight']; ?></td><?php endif; ?>
                    <?php if($passenger['to']): ?><td><?php echo $passenger['todate']; ?></td><?php endif; ?>
                    <?php if(!$passenger['back'] && !$passenger['to']): ?><td></td><td></td><?php endif; ?>
                    <td><?php if($passenger['pdf']) echo '<a class="tkt-download form-submit" href="'.$passenger['pdf'].'" DOWNLOAD>'.$passenger['pdfname'].'</a>'; ?></td>
                    <?php if(!tickets_is_agent($user)): ?><td><?php if(isset($agent->FullName)) {echo $agent->FullName;} ?></td><?php endif; ?>
                    <?php if(isset($agent->FullName) && isset($agents[$agent->FullName])) $agents[$agent->FullName] = $agents[$agent->FullName] + 1; ?>
                </tr>
                <?php $num++; ?>
            <?php endforeach; ?>
        <?php endif; ?>
    </tbody>
</table>
<?php if(count($data)): ?>
    <div class="tax-flight-actions-bottom">
        <a href="#" class="pax-tkt form-submit">Выписать авиабилет</a>
        <a href="#" class="pax-edit form-submit">Редактировать пассажира</a>
        <a href="#" class="pax-delete form-submit">Удалить пассажиров</a>
        <a href="#" class="pax-download form-submit">Скачать отмеченные выписанные билеты</a>
        <a href="#" class="pax-tkt-no form-submit">Выбрать всех невыписанных пассажиров</a>
    </div>
<?php endif; ?>
<?php if(count($agents) && !tickets_is_agent($user)): ?>
    <div class="tax-flight-actions-bottom" style="position: relative;top:15px;width: 100%;float: left;">
        Статистика: 
        <?php foreach($agents as $name=>$total): ?>
            <?php echo $name.': <b>'.$total.'</b> пас.;  '; ?>
        <?php endforeach; ?>
    </div>
<?php endif; ?>