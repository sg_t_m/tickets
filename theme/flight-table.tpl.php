<table class="display flight-data-table" cellspacing="0" width="100%">
    <thead>
        <tr>
            <th rowspan="2" class="no-sort">Agents</th>
            <th rowspan="2">Block netto</th>
            <th rowspan="2">Block fact</th>
            <th rowspan="2">Tariff RT/2</th>
            <th rowspan="2">Tariff OW</th>
            <th rowspan="2">Total per Block</th>
            <th colspan="3">PAX reserv</th>
            <th colspan="3">PAX fact</th>
            <th colspan="3">TAX</th>
            <th rowspan="2">Catering</th>
            <th rowspan="2">Total PAX reserve</th>
            <th rowspan="2">Total PAX fact</th>
            <th rowspan="2">Total Catering</th>
            <th rowspan="2">Total TAX</th>
            <?php if(!tickets_is_agent($user)): ?><th rowspan="2"></th><?php endif; ?>
        </tr>
        <tr>
            <th class="no-sort">ADL</th>
            <th class="no-sort">CHD</th>
            <th class="no-sort">INF</th>
            <th class="no-sort">ADL</th>
            <th class="no-sort">CHD</th>
            <th class="no-sort">INF</th>
            <th class="no-sort">ADL</th>
            <th class="no-sort">CHD</th>
            <th class="no-sort">INF</th>
        </tr>
    </thead>
   <tfoot>
        <?php foreach($data as $key=>$row): ?>
            <?php if(!$key): ?>        
                <tr>
                    <?php foreach($row as $name=>$value): ?>
                        <td data-name="<?php echo $name; ?>"><?php echo $value; ?></td>
                    <?php endforeach; ?>
                    <?php if(!tickets_is_agent($user)): ?><td></td><?php endif; ?>
                </tr>
            <?php endif; ?>
        <?php endforeach; ?>        
    </tfoot>    
    <tbody>
        <?php foreach($data as $uid=>$row): ?>
            <?php if($uid): ?>        
                <tr>
                    <?php foreach($row as $name=>$value): ?>
                        <td data-name="<?php echo $name; ?>" class="<?php echo drupal_strtolower(str_replace(' ','_',$name)); ?>">
                            <span><?php echo $value; ?></span>
                            <input type="text" class="form-text" value="<?php echo $value; ?>">
                        </td>
                    <?php endforeach; ?>
                    <?php if(!tickets_is_agent($user)): ?>
                        <td>
                            <a class="edit-row" href="#" data-nid="<?php echo $node->nid; ?>" data-uid="<?php echo $uid; ?>"><i class="fa fa-pencil-square" aria-hidden="true"></i></a>
                            <a class="edit-save" href="#" data-nid="<?php echo $node->nid; ?>" data-uid="<?php echo $uid; ?>"><i class="fa fa-check-square" aria-hidden="true"></i></a>
                        </td>
                    <?php endif; ?>
                </tr>
            <?php endif; ?>
        <?php endforeach; ?>        
    </tbody>
</table>