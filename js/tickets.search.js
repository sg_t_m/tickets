jQuery.browser = {};
(function () {
    jQuery.browser.msie = false;
    jQuery.browser.version = 0;
    if (navigator.userAgent.match(/MSIE ([0-9]+)\./)) {
        jQuery.browser.msie = true;
        jQuery.browser.version = RegExp.$1;
    }
})();

(function ($, Drupal) {
  Drupal.behaviors.ticketssearch = {
    attach: function(context, settings) {
        if($('.date-picker').length) {
            $('.date-picker').datepicker({dateFormat: "dd/mm/yy"});
        }
        $('.reports-submit').once('reports', function() {
            $('.reports-submit').on('click', function(event){
                event.stopPropagation();
                var btn = $(this);
                btn.val('Формируем отчет...');
                $.ajax({
                    type: "POST",
                    url: Drupal.settings.tickets.ajax+'/report',
                    data: {fields:$('#tickets-reports-form').serialize()},
                    success: function(data){
                        btn.val('Формировать');
                        if (data.result) {
                            window.open(data.result, '_blank');
                        }
                    }
                 });
                return false;
            });
        });
    }
  };
})(jQuery, Drupal);        

var Calendar = '';

(function ($) {
    $(function () {
        if($('.DateFrom').length){
            moment() instanceof moment;
            moment.locale('ru', {
                months : [
                    "Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль",
                    "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"
                ]
            });        
            var thisMonth = moment().format('YYYY-MM');           
            var Calendar = $('.cal3').clndr({
                lengthOfTime: {
                    months: 2,
                    interval: 1
                },
                constraints: {
                    startDate: Drupal.settings.tickets.startDate
                },
                weekOffset: 1,
                daysOfTheWeek: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
                clickEvents: {
                    click: function (target) {
                        $('.day.event.selected').removeClass('selected');
                        if(target.events.length && $(target.element).hasClass('event') && !$(target.element).hasClass('adjacent-month')) {
                            $(target.element).addClass('selected');
                            var flights = $.map(target.events, function(item) {
                                return {
                                    date: item.date,
                                    nid: item.nid
                                };
                            });
                            if ($('.DateFrom.active').length) {
                                $('.DateFrom.active').val(flights[0].date);
                                $('.DateFrom.active').attr('rel', flights[0].nid);
                                $('.DateFrom').removeClass('active');
                            }
                            if ($('.DateTo.active').length) {
                                $('.DateTo.active').val(flights[0].date);
                                $('.DateTo.active').attr('rel', flights[0].nid);
                                $('.DateTo').removeClass('active');
                            }
                            $('.popup-calendar').addClass('hidden');
                            $('.popup-calendar').hide();
                        }                    
                    },
                },
                forceSixRows: true,
                template: $('#mini-clndr-template').html()
            });
            function LoadCalendarEvents(direction) {
                var AirportFrom = $('select[name=AirportFrom] option:selected').val();
                var AirportTo = $('select[name=AirportTo] option:selected').val();
                $.ajax({
                    type: "POST",
                    url: Drupal.settings.tickets.ajax+'/load-flights-days',
                    data: {AirportFrom:AirportFrom,AirportTo:AirportTo, direction:direction},
                    success: function(data){
                        Calendar.setEvents(data.result);                               
                    }
                });
            }
            $('.DateFrom, .DateTo').on('click', function(event){
                event.stopPropagation();
                $(this).addClass('active');
                if ($(this).hasClass('DateFrom')) {
                    LoadCalendarEvents('from');
                } else {
                    LoadCalendarEvents('to');
                }
                $('.search-person').addClass('hidden');
                var position = $(this).position();
                $('.popup-calendar').css({
                    'left' : position.left,
                    'top' : position.top+30,
                });
                $('.popup-calendar').removeClass('hidden');
                $('.popup-calendar').show();                
                return false;
            });              
        }
        
        $('.booking .form-actions .form-submit').on('click', function(event){
            event.stopPropagation();
            var nidfrom = $('.DateFrom').attr('rel');
            var nidto = $('.DateTo').attr('rel');
            if (nidfrom !== undefined || nidto !== undefined) {
                if (nidfrom !== '' || nidto !== '') {
                    $.ajax({
                        type: "POST",
                        url: Drupal.settings.tickets.ajax+'/load-flights-user',
                        data: {nidfrom:nidfrom,nidto:nidto},
                        success: function(data){
                            $('.booking-tickets').html(data.result);                                
                            $('.order-tickets-user').on('click', function(event){
                                event.preventDefault();
                                var nidfrom = $(this).data('nidfrom');
                                var nidto = $(this).data('nidto');
                                var ADL = $('.search-adl').val();
                                var CHD = $('.search-chd').val();
                                var INF = $('.search-inf').val();
                                $('#tickets-order-form input[name=nidfrom]').val(nidfrom);
                                $('#tickets-order-form input[name=nidto]').val(nidto);
                                $('#tickets-order-form input[name=ADL]').val(ADL);
                                $('#tickets-order-form input[name=CHD]').val(CHD);
                                $('#tickets-order-form input[name=INF]').val(INF);
                                $('#tickets-order-form').show();
                                $('.confirm-box').hide();                                
                                $('.popup-wrap').fadeIn(250);
                                $('.popup-box').removeClass('transform-out').addClass('transform-in');
                                return false;
                            });
                            $('#tickets-order-form .order-ticket-submit').on('click', function(event){
                                var errors = 0;
                                var OrderName = $('#tickets-order-form input[name=name]');
                                var OrderPhone = $('#tickets-order-form input[name=phone]');
                                var OrderMail = $('#tickets-order-form input[name=mail]');
                                OrderName.removeClass('error');
                                OrderPhone.removeClass('error');
                                OrderMail.removeClass('error');
                                if (!OrderName.val()) {
                                    OrderName.addClass('error');
                                    errors++;
                                }
                                if (!OrderPhone.val()) {
                                    OrderPhone.addClass('error');
                                    errors++;
                                }
                                if (!OrderMail.val()) {
                                    OrderMail.addClass('error');
                                    errors++;
                                }
                                if (!errors) {
                                    $.ajax({
                                        type: "POST",
                                        url: Drupal.settings.tickets.ajax+'/order-ticket',
                                        data: {fields:$('#tickets-order-form').serialize()},
                                        success: function(data){
                                            $('#tickets-order-form').hide();
                                            $('.confirm-box').show();
                                            setTimeout(function(){
                                                $('.popup-wrap').fadeOut(500);
                                                $('.popup-box').removeClass('transform-in').addClass('transform-out');  
                                            }, 2000);
                                        }
                                    });
                                }
                                return false;
                            });
                            $('.popup-close, #tickets-order-form #edit-undo').on('click', function(event){
                                $('.popup-wrap').fadeOut(500);
                                $('.popup-box').removeClass('transform-in').addClass('transform-out');
                                event.preventDefault();
                                return false;
                            });
                        }
                    });
                }
            }
            return false;
        });
        function declOfNum(number, titles){  
            cases = [2, 0, 1, 1, 1, 2];  
            return titles[ (number%100>4 && number%100<20)? 2 : cases[(number%10<5)?number%10:5] ];  
        }            
        $('.persons-select .plus, .persons-select .minus').on('click', function(event){
            var field = $(this).parent().find('.form-text');
            if ($(this).hasClass('plus')) {
                if (parseInt(field.val())<9) {
                    field.val((parseInt(field.val())+1));
                }
            } else {
                if (field.hasClass('search-adl')) {
                    if (parseInt(field.val())>1) {
                        field.val((parseInt(field.val())-1));
                    }                        
                } else {
                    if (parseInt(field.val())>0) {
                        field.val((parseInt(field.val())-1));
                    }
                }
            }
            var ADL = parseInt($('.form-item-ADL .form-text').val());
            var CHD = parseInt($('.form-item-CHD .form-text').val());
            var INF = parseInt($('.form-item-INF .form-text').val());
            var total = ADL+CHD+INF;
            TotalTitle = total+' '+declOfNum(total,['человек','человека','человек']);
            $('#edit-persons').val(TotalTitle);                
            return false;
        });                 
    });
})(jQuery);

(function ($, Drupal) {
  Drupal.behaviors.tickets = {
    attach: function(context, settings) {
            
            /* Selectors */
            function ClearSearchResults() {
                $('.DateFrom').val('');
                $('.DateFrom').attr('rel', '');
                $('.DateTo').val('');
                $('.DateTo').attr('rel', '');
                $('.booking-tickets').html('');
            }
            $('body').on('click', function(event){
                $('.search-person').addClass('hidden');
                $('.popup-calendar').hide();
                $('.DateFrom').removeClass('active');
                $('.DateTo').removeClass('active');
            });
            $('.search-person').on('click', function(event){
                event.stopPropagation();
                $('.popup-calendar').hide();
                $('.search-person').removeClass('hidden');
                return false;
            });            
            $('input[name=direction]').on('change', function(event){
                ClearSearchResults();
                if($(this).val()==1) {
                    if($('select[name=AirportFrom] option:selected').val()!=0 && $('select[name=AirportTo] option:selected').val()!=0) {
                        $('.form-item-DateFrom').show();
                        $('.form-item-DateTo').show();
                    }
                } else {
                    $('.form-item-DateTo').hide();
                }
            });
            $('select[name=AirportFrom]').on('change', function(event){
                ClearSearchResults();
                if($('select[name=AirportFrom] option:selected').val()!=0 && $('select[name=AirportTo] option:selected').val()!=0) {
                    $('.form-item-DateFrom').show();
                } else {
                    $('.form-item-DateFrom').hide();
                }
            });
            $('select[name=AirportTo]').on('change', function(event){
                ClearSearchResults();                
                if($('select[name=AirportFrom] option:selected').val()!=0 && $('select[name=AirportTo] option:selected').val()!=0) {
                    $('.form-item-DateFrom').show();
                    if($('input[name=direction]:checked').val()!=0) {                        
                        $('.form-item-DateTo').show();
                    }
                } else {
                    $('.form-item-DateFrom').hide();
                    $('.form-item-DateTo').hide();                    
                }
            });
            $('.popup-calendar').on('click', function(event){
                event.stopPropagation();
                if (!$(this).hasClass('hidden')) {
                    $('.popup-calendar').show();   
                }                
                return false;
            });                           
    }
  };
})(jQuery, Drupal);