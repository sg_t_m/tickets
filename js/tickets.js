(function ($, Drupal) {
  Drupal.behaviors.tickets = {
    attach: function(context, settings) {        
        function LoadPassengersTable(AddForm) {
            var nid = AddForm.parent().data('nid');
            var date = AddForm.parent().data('date');                
            $.ajax({
                type: "POST",
                url: Drupal.settings.tickets.ajax+'/load-passengers-table',
                data: {nid:nid,date:date},
                success: function(data){
                    AddForm.parent().parent().find('.tax-passangers-table-inner').html(data.result);
                    AddForm.parent().parent().find('.flight-passangers-table').DataTable({
                        paging: false,
                        searching: false,
                        bInfo: false,
                        order: [],
                        language: {
                            "emptyTable": "Пассажиры отсутствуют"
                        },
                        columnDefs: [{
                            "targets": 'no-sort',
                            "orderable": false,
                        }]
                    });
                    AddForm.parent().parent().find('.pax-delete').on('click', function(event){
                        var passengers = AddForm.parent().parent().find('.tax-passangers-table-inner input:checked[name="passengers[]"]').map(function(){return $(this).val();}).get();
                        $.ajax({
                            type: "POST",
                            url: Drupal.settings.tickets.ajax+'/passengers-delete',
                            data: {passengers:passengers},
                            success: function(data){
                                LoadPassengersTable(AddForm);
                            }
                        });                                    
                        return false;
                    });
                    AddForm.parent().parent().find('.pax-edit').on('click', function(event){
                      if (AddForm.parent().parent().find('.tax-passangers-table-inner input:checked[name="passengers[]"]').length) {
                        var passengers = AddForm.parent().parent().find('.tax-passangers-table-inner input:checked[name="passengers[]"]').map(function(){return $(this).val();}).get();
                        $.ajax({
                            type: "POST",
                            url: Drupal.settings.tickets.ajax+'/edit-passenger',
                            data: {passengers:passengers},
                            success: function(data){
                              $('.edit-area-form').html(data.result);
                              $('.edit-area, .edit-area-form').show();
                              AddForm.parent().slideUp('fast');
                              $('.animatedModal-on').animate({scrollTop: 0},'slow');
                              var EditForm = $('.edit-area .node-form');
                              var BirthdayInput = EditForm.find('input[name="field_passenger_birthday[und][0][value][date]"]');
                              var ExpireInput = EditForm.find('input[name="field_passenger_document_expire[und][0][value][date]"]');
                              BirthdayInput.attr('id', 'field_passenger_birthday_edit');
                              ExpireInput.attr('id', 'field_passenger_document_expire_edit');
                              BirthdayInput.datepicker({dateFormat: "dd/mm/yy"});
                              ExpireInput.datepicker({dateFormat: "dd/mm/yy"});
                              EditForm.find('.form-actions.form-wrapper').append(' <input type="submit" id="edit-undo-edit" name="op" value="Отменить" class="form-submit">');
                              $('#edit-undo-edit').on('click', function(event){
                                  EditForm.parent().parent().slideUp('fast');
                                  EditForm.find('.form-actions.form-wrapper').find('.error').remove();
                                  EditForm.trigger('reset');
                                  return false; 
                              });
                              EditForm.find('.form-item-title').find('.form-text').liTranslit({'caseType':'upper'});
                              EditForm.find('.form-submit:not(#edit-undo)').on('click', function(event){
                                  var btn = $(this);
                                  btn.val('Сохраняем...');
                                  var title = EditForm.find('input[name="title"]').val();
                                  var expire = EditForm.find('input[name="field_passenger_document_expire[und][0][value][date]"]').val();
                                  var birthday = EditForm.find('input[name="field_passenger_birthday[und][0][value][date]"]').val();
                                  var resident = EditForm.find('select[name="field_passenger_resident[und]"] option:selected').val();
                                  var status = EditForm.find('input:checked[name="field_passenger_status[und]"]').val();
                                  var sex = EditForm.find('input:checked[name="field_passenger_sex[und]"]').val();
                                  var document = EditForm.find('input:checked[name="field_passenger_document[und]"]').val();
                                  var num = EditForm.find('input[name="field_passenger_document_num[und][0][value]"]').val();
                                  $.ajax({
                                      type: "POST",
                                      url: Drupal.settings.tickets.ajax+'/save-passenger',
                                      data: {nid:data.nid,resident:resident,status:status,sex:sex,document:document,num:num,birthday:birthday,expire:expire,title:title},
                                      success: function(data){
                                        EditForm.parent().parent().slideUp('fast');
                                        EditForm.find('.form-actions.form-wrapper').find('.error').remove();
                                        EditForm.trigger('reset');
                                        LoadPassengersTable(AddForm);
                                        return false; 
                                      }
                                   });
                                  return false;
                              });                              
                              
                            }
                        });                                    
                        return false;
                      }
                    });
                    AddForm.parent().parent().find('.pax-tkt').on('click', function(event){
                        var passengers = AddForm.parent().parent().find('.tax-passangers-table-inner input:checked[name="passengers[]"]').map(function(){return $(this).val();}).get();
                        $.ajax({
                            type: "POST",
                            url: Drupal.settings.tickets.ajax+'/passengers-tkt',
                            data: {passengers:passengers},
                            success: function(data){
                              LoadPassengersTable(AddForm);
                            }
                        });                                    
                        return false;
                    });
                    AddForm.parent().parent().find('.pax-tkt-no').on('click', function(event){
                      AddForm.parent().parent().find('.tax-passangers-table-inner input[name="passengers[]"]').each(function( index, value ) {
                        if(!$(this).parent().parent().find('.tkt-download').length){
                          $(this).prop('checked', true);
                        }
                      });
                      return false;
                    });
                    AddForm.parent().parent().find('.pax-download').on('click', function(event){
                        var link = $(this);
                        link.text('Формирование билетов...');
                        var passengers = AddForm.parent().parent().find('.tax-passangers-table-inner input:checked[name="passengers[]"]').map(function(){return $(this).val();}).get();
                        $.ajax({
                            type: "POST",
                            url: Drupal.settings.tickets.ajax+'/passengers-tkt-download',
                            data: {passengers:passengers},
                            success: function(data){
                                link.text('Скачать все выписанные билеты');
                                window.open(data.result, '_blank');
                            }
                        });                                    
                        return false;
                    });
                }
            });            
        }
        function InitAddPassenger(AddForm) {
            var nid = AddForm.parent().data('nid');
            var date = AddForm.parent().data('date');
            $('.error-import').hide();
            if (!$('.pax-import').hasClass('dz-clickable')) {                        
              $('.pax-import').dropzone({
                  params: {nid: nid, date: date},   
                  url: Drupal.settings.tickets.ajax+"/pax-import-upload",
                  autoDiscover: false,
                  acceptedFiles: '.xls,.xlsx',
                  uploadMultiple: false,
                  init: function() {
                      this.on("addedfile", function(file) {
                          AddForm.parent().parent().find('.pax-import').text('Импорт в процессе...');
                      });
                      this.on('success', function(file, json) {                        
                          if(json.result){
                            if(json.hasOwnProperty('error')){
                              $('.error-import').html(json.result);
                              $('.error-import').show();
                              AddForm.parent().parent().find('.pax-import').text('Импорт из XLS');
                            } else {
                              LoadPassengersTable(AddForm);
                              AddForm.parent().parent().find('.pax-import').text('Импорт из XLS');
                            }
                          }
                      });
                  }
              });
            }
            AddForm.find('.date-clear').datepicker({dateFormat: "dd/mm/yy"});
            $('.pax-export').on('click', function(event){
                var passengers = AddForm.parent().parent().find('.tax-passangers-table-inner input[name="passengers[]"]').map(function(){return $(this).val();}).get();
                var nid = $(this).data('nid');
                $.ajax({
                    type: "POST",
                    url: Drupal.settings.tickets.ajax+'/passengers-export',
                    data: {passengers:passengers, nid:nid},
                    success: function(data){
                      window.open(data.result, '_blank');
                    }
                });                                                    
                return false;
            });
            $('.pax-add').on('click', function(event){
                AddForm.parent().slideDown('fast');
                AddForm.trigger('reset');
                return false;
            });
            AddForm.find('.form-actions.form-wrapper').append(' <input type="submit" id="edit-undo" name="op" value="Отменить" class="form-submit">');
            $('#edit-undo').on('click', function(event){
                AddForm.parent().slideUp('fast');
                AddForm.find('.form-actions.form-wrapper').find('.error').remove();
                AddForm.trigger('reset');
               return false; 
            });
            AddForm.find('.form-item-title').find('.form-text').liTranslit({'caseType':'upper'});
            AddForm.find('.form-submit:not(#edit-undo):not(#edit-undo-edit)').on('click', function(event){
                var btn = $(this);
                btn.val('Добавляем...');
                $.ajax({
                    type: "POST",
                    url: Drupal.settings.tickets.ajax+'/add-passenger',
                    data: {nid:nid,date:date,fields:AddForm.serialize()},
                    success: function(data){
                        if (data.result==true) {
                            AddForm.find('.form-actions.form-wrapper').find('.error').remove();
                            AddForm.trigger('reset');
                            LoadPassengersTable(AddForm);
                            btn.val('Сохранить');
                        } else {
                            if (!AddForm.find('.form-actions.form-wrapper').find('.error').length) {
                                AddForm.find('.form-actions.form-wrapper').append('<div class="error">Вы должны заполнить все поля!</div>');
                                btn.val('Сохранить');
                            }                            
                        }
                    }
                 });
                return false;
            });
        }
        /* Load Flight Data */
        function LoadFlightData(nid,date) {
            $.ajax({
                type: "POST",
                url: Drupal.settings.tickets.ajax+'/load-flight-data',
                data: {nid:nid,date:date},
                success: function(data){
                    var InnerWidth = $('.container').width();
                    var InnerPaddings = 40;
                    $('.flight-data').html(data.result);
                    $('.flight-data-tables').css({
                        'left' : '-'+(($(document).width()-InnerWidth)/2-InnerPaddings)+'px',
                        'width': $(document).width()-(InnerPaddings*2),
                    });
                    $('.flight-data-table').DataTable({
                        paging: false,
                        searching: false,
                        bInfo: false,
                        order: [],
                        columnDefs: [{
                            "targets": 'no-sort',
                            "orderable": false,
                        }]
                    });
                    $('.edit-save').on('click', function(event){
                        var nid = $(this).data('nid');
                        var uid = $(this).data('uid');
                        var block_fact = $(this).parent().parent().find('.block_fact').find('.form-text').val();
                        var pax_fact_adl = $(this).parent().parent().find('.pax_fact_adl').find('.form-text').val();
                        var pax_fact_chd = $(this).parent().parent().find('.pax_fact_chd').find('.form-text').val();
                        var pax_fact_inf = $(this).parent().parent().find('.pax_fact_inf').find('.form-text').val();
                        $.ajax({
                            type: "POST",
                            url: Drupal.settings.tickets.ajax+'/save-flight',
                            data: {nid:nid,uid:uid,block_fact:block_fact,pax_fact_adl:pax_fact_adl,pax_fact_chd:pax_fact_chd,pax_fact_inf:pax_fact_inf},
                            success: function(data){
                                var nid = $('#animatedModal').data('nid');
                                var date = $('#animatedModal').data('date');
                                LoadFlightData(nid,date);
                            }
                        });                                                            
                        return false;
                    });
                    $('.edit-row').on('click', function(event){
                        $(this).hide();
                        $(this).parent().find('.edit-save').show();
                        $(this).parent().parent().find('.block_fact').find('span').hide();
                        $(this).parent().parent().find('.block_fact').find('.form-text').show();
                        $(this).parent().parent().find('.pax_fact_adl').find('span').hide();
                        $(this).parent().parent().find('.pax_fact_adl').find('.form-text').show();
                        $(this).parent().parent().find('.pax_fact_chd').find('span').hide();
                        $(this).parent().parent().find('.pax_fact_chd').find('.form-text').show();
                        $(this).parent().parent().find('.pax_fact_inf').find('span').hide();
                        $(this).parent().parent().find('.pax_fact_inf').find('.form-text').show();                        
                        return false;
                    });
                    $(".pax-add-edit").animatedModal({
                        modalTarget:'animatedModal', 
                        animatedIn:'zoomIn',
                        animatedOut:'bounceOut',
                        color:'rgba(255,255,255, 1)',
                        beforeOpen: function() {
                            InitAddPassenger($('#animatedModal .add-area .node-passenger-form'));
                            LoadPassengersTable($('#animatedModal .add-area .node-passenger-form'));
                        },
                        afterClose: function() {
                            var nid = $('#animatedModal').data('nid');
                            var date = $('#animatedModal').data('date');
                            LoadFlightData(nid,date);                            
                        }
                    });                    
                    $(".pax-add-edit-back").animatedModal({
                        modalTarget:'animatedModalBack', 
                        animatedIn:'zoomIn',
                        animatedOut:'bounceOut',
                        color:'rgba(255,255,255, 1)',
                        beforeOpen: function() {
                            InitAddPassenger($('#animatedModalBack .add-area .node-passenger-form'));
                            LoadPassengersTable($('#animatedModalBack .add-area .node-passenger-form'));
                        },
                        afterClose: function() {
                            var nid = $('#animatedModal').data('nid');
                            var date = $('#animatedModal').data('date');
                            LoadFlightData(nid,date);                            
                        }
                    });                    
                    $('html,body').animate({scrollTop: $('.flight-data').offset().top-40},'slow');
                }
            });            
        }
        
        /* Init Calendars */
        moment() instanceof moment;
        moment.locale('ru', {
            months : [
                "Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль",
                "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"
            ]
        });        
        var thisMonth = moment().format('YYYY-MM');        
        var Calendar = $('.cal3').clndr({
            lengthOfTime: {
                months: 3,
                interval: 1
            },
            /*
            constraints: {
                startDate: Drupal.settings.tickets.startDate
            },
            */
            events: Drupal.settings.tickets.flights,
            weekOffset: 1,
            daysOfTheWeek: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
            clickEvents: {
                click: function (target) {
                    $('.day.event.selected').removeClass('selected');
                    if(target.events.length && $(target.element).hasClass('event') && !$(target.element).hasClass('adjacent-month')) {
                        $(target.element).addClass('selected');
                        var flights = $.map(target.events, function(item) {
                            return {
                                date: item.date,
                                nid: item.nid
                            };
                        });
                        $.ajax({
                            type: "POST",
                            url: Drupal.settings.tickets.ajax+'/load-flights',
                            data: {flights:flights},
                            success: function(data){
                                $('.flights').html(data.result);                                
                                $('.boarding-pass').on('click', function(event){
                                    var nid = $(this).data('nid');
                                    var date = $(this).data('date');
                                    LoadFlightData(nid,date);
                                    return false;
                                });
                            }
                        });
                    }                    
                },
            },
            forceSixRows: true,
            template: $('#mini-clndr-template').html()
        });        
        $(document).keydown( function(e) {
            if (e.keyCode == 37) {
                Calendar.back();
            }
            if (e.keyCode == 39) {
                Calendar.forward();
            }
        });
    }
  };
})(jQuery, Drupal);        